module LookAndFeel
  class Engine < ::Rails::Engine

    initializer :assets do |config|
    end

    initializer 'new_relic.locales' do |app|
      if app.config.i18n.fallbacks.blank?
        app.config.i18n.fallbacks = [:en]
      end
    end
  end
end