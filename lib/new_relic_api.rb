require 'gruff'

module NewRelicApi
  MEGABYTE 	= 1024.0 * 1024.0
  GIGABYTE	= 1073741824

  class Client
    @api_key
    @api_version

    BASE_API_URL = 'https://api.newrelic.com'

    def initialize(api_key, api_version ='v2')
      @api_key 	= api_key
      @api_version 	= api_version
    end

    def api_url
      "#{BASE_API_URL}/#{@api_version}"
    end

    def api_key
      @api_key
    end

    def api_version
      @api_version
    end

    def self.hash_to_array_params(hash, param_name)
      params = Hash.new

      hash.each do |k,v|
        params["#{param_name}[#{k}]"] = v
      end

      params
    end

    def graph_memory(server, memory_metrics)
      g = Gruff::Line.new(800)
      g.title = "#{server['name']} - Memory Usage"

      label_count = 0
      data = Array.new
      labels = Hash.new
      memory_metrics['timeslices'].each do |timeslice|
        label = Time.parse(timeslice['from']).strftime('%H:%M')



        labels[label_count] = label
        data << timeslice['values']['average_value'].to_i/GIGABYTE

        label_count = label_count+1
      end

      g.labels = labels
      g.data 'Memory Usage (in GB)', data

      g
    end

    def graph_cpu(server, memory_metrics)
      g = Gruff::Line.new(800)
      g.title = "#{server['name']} - CPU Usage"

      label_count = 0
      data = Array.new
      labels = Hash.new
      memory_metrics['timeslices'].each do |timeslice|
        label = Time.parse(timeslice['from']).strftime('%H:%M')



        labels[label_count] = label
        data << timeslice['values']['average_value']


        label_count = label_count+1
      end

      g.labels = labels
      g.data 'CPU Usage (in %)', data

      g
    end

    def graph_disk(server, metrics, title_str, data_label)
      g = Gruff::Line.new(800)
      g.title = "#{server['name']} - #{title_str}"

      label_count = 0
      data = Array.new
      labels = Hash.new
      metrics['timeslices'].each do |timeslice|
        label = Time.parse(timeslice['from']).strftime('%H:%M')



        labels[label_count] = label
        data << timeslice['values']['average_value'].to_i/GIGABYTE

        label_count = label_count+1
      end

      g.labels = labels
      g.data data_label, data

      g
    end
  end

  class ServerAPI
    @client

    def initialize(client)
      @client = client
    end

    def servers
      parse_json(RestClient.get "#{@client.api_url}/servers.json", {'X-Api-Key' => @client.api_key})
    end

    def servers_by_filter(filters = {})
      parse_json(RestClient.get "#{@client.api_url}/servers.json", {'X-Api-Key' => @client.api_key, :params => Client.hash_to_array_params(filters, 'filter')})
    end

    def metric_names(server_id)
      parse_json(RestClient.get "#{@client.api_url}/servers/#{server_id}/metrics.json", {'X-Api-Key' => @client.api_key})
    end

    def metric_data(server_id, filters = {})
      begin
        parse_json(RestClient.get "#{@client.api_url}/servers/#{server_id}/metrics/data.json", {'X-Api-Key' => @client.api_key, :params => filters})
      rescue => e
        puts e.response
      end

    end

    def parse_json(json)
      JSON.parse(json)
    end
  end
end
