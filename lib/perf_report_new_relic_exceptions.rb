module PerfReportNewRelicExceptions
  module API
    class MissingKey < Exception
      def initialize(message)
        super
        @message = message
      end
    end

    class InvalidKey < Exception
      def initialize(message)
        super
        @message = message
      end
    end
  end
end