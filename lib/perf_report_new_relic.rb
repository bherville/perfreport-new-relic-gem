require 'perf_report_new_relic/version'
require 'perf_report_new_relic_exceptions'
require 'new_relic_api'

module PerfReportNewRelic
  class GraphSource
    @graph_command
    @graph_types
    @time_zone
    @parameters
    @logger
    @client

    def initialize(report_hash, logger = Rails.logger)
      @graph_command  = report_hash[:graph_command]
      @graph_types    = report_hash[:graph_types]
      @time_zone      = report_hash[:time_zone]
      @parameters     = report_hash[:module_parameters]

      @logger         = report_hash[:logger]
      @logger         ||= Rails.logger

      PerfReportNewRelicExceptions::API::MissingKey.new('New Relic REST API Key Missing!') unless @parameters[:rest_api_key]

      @client = NewRelicApi::Client.new(@parameters[:rest_api_key])
    end

    def generate_graphs(tmp_graph_dir, servers, file_type = :png)
      server_api = NewRelicApi::ServerAPI.new(@client)

      server_graph = Hash.new
      @logger.info("Generating Graphs to: #{tmp_graph_dir}")

      Dir.mkdir tmp_graph_dir unless Dir.exists?(tmp_graph_dir)

      servers.each do |server|
        @logger.debug("Generating Graphs for: #{server.name}")
        server_graph[server.name] = Array.new

        api_server = server_api.servers_by_filter(:name => server.name.split('.')[0])['servers'][0]

        next unless api_server

        @graph_types.each do |graph_type|
          @logger.debug("Generating Graphs: #{graph_type.name}")
          case graph_type.name
            when 'cpu_graph'
              cpu_metrics = server_api.metric_data(api_server['id'], {'names[]' => 'System/CPU/System/percent', :from => 1.day.ago.to_s, :period => 7200})['metric_data']['metrics'][0]
              cpu_graph = @client.graph_cpu api_server, cpu_metrics

              cpu_graph_path = File.join(tmp_graph_dir, "#{server.name}_cpu_graph#{graph_extension(file_type)}")
              cpu_graph.write(cpu_graph_path)

              server_graph[server.name] << cpu_graph_path
            when 'memory_graph'
              memory_metrics = server_api.metric_data(api_server['id'], {'names[]' => 'System/Memory/Used/bytes', :from => 1.day.ago.to_s, :period => 7200})['metric_data']['metrics'][0]
              memory_graph = @client.graph_memory api_server, memory_metrics

              memory_graph_path = File.join(tmp_graph_dir, "#{server.name}_memory_graph#{graph_extension(file_type)}")
              memory_graph.write(memory_graph_path)

              server_graph[server.name] << memory_graph_path
            when 'disk_all_read_graph'
              metrics = server_api.metric_data(api_server['id'], {'names[]' => 'System/Disk/All/Reads/bytes/sec', 'values[]' => 'per_second', :from => 1.day.ago.to_s, :period => 7200})['metric_data']['metrics'][0]
              graph = @client.graph_disk api_server, metrics, 'Disk Read IO', 'Disk Read IO (bytes per second)'

              graph_path = File.join(tmp_graph_dir, "#{server.name}_disk_all_read_graph#{graph_extension(file_type)}")
              graph.write(graph_path)

              server_graph[server.name] << graph_path
            when 'disk_all_write_graph'
              metrics = server_api.metric_data(api_server['id'], {'names[]' => 'System/Disk/All/Writes/bytes/sec', 'values[]' => 'per_second', :from => 1.day.ago.to_s, :period => 7200})['metric_data']['metrics'][0]
              graph = @client.graph_disk api_server, metrics, 'Disk Write IO', 'Disk Write IO (bytes per second)'

              graph_path = File.join(tmp_graph_dir, "#{server.name}_disk_all_write_graph#{graph_extension(file_type)}")
              graph.write(graph_path)

              server_graph[server.name] << graph_path
          end
        end
      end

      server_graph
    end

    private
    def graph_extension(file_type)
      case file_type
        when :png
          '.png'
        when :svg
          '.svg'
        else
          '.png'
      end
    end
  end
end
