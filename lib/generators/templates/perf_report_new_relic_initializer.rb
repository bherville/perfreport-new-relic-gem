ActiveSupport::Inflector.inflections do |inflect|
  inflect.uncountable 'NewRelic'
  inflect.uncountable 'PerfReportNewRelic'
end