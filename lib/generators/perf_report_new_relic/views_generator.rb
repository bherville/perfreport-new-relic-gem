module PerfReportNewRelic
  module Generators
    class ViewsGenerator < ::Rails::Generators::Base
      source_root File.expand_path('../../../../app/views', __FILE__)

      desc 'Copies default new relic views.'

      def manifest
        directory 'new_relic', 'app/views/new_relic'
      end
    end
  end
end