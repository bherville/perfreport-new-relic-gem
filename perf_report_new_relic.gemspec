# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'perf_report_new_relic/version'

Gem::Specification.new do |spec|
  spec.name          = 'perf_report_new_relic'
  spec.version       = PerfReportNewRelic::VERSION
  spec.authors       = ['Brad Herring']
  spec.email         = ['brad@bherville.com']

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  spec.summary       = %q{New Relic Source Module for PerfReport.}
  spec.description   = %q{New Relic Source Module for PerfReport.}
  spec.homepage      = 'https://bitbucket.org/bherring/perfreport-new-relic-gem'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.8'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_runtime_dependency 'gruff'
end
